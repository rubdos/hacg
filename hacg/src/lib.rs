use home_assistant_rest::{get::StatesEntityResponse, Client};

#[macro_export]
macro_rules! state {
    // sensor
    (
        entity_type: sensor,
        entity_name: $entity_name:ident,
        entity_id: $entity_id:literal,
    ) => {
        #[allow(non_camel_case_types)]
        pub struct $entity_name;
        impl ::hacg::Sensor for $entity_name {
            fn entity_id(&self) -> &'static str {
                $entity_id
            }
        }
    };

    // BLANKET/fallback
    (
        entity_type: $entity_type:ident,
        entity_name: $entity_name:ident,
        entity_id: $entity_id:literal,
    ) => {
        // pub mod $entity_name {}
    };
}

pub trait Sensor {
    fn entity_id(&self) -> &'static str;
}

#[async_trait::async_trait(?Send)]
pub trait SensorExt {
    async fn sensor<S: Sensor>(
        &self,
        _s: S,
    ) -> Result<StatesEntityResponse, home_assistant_rest::errors::Error>;
}

#[async_trait::async_trait(?Send)]
impl SensorExt for Client {
    async fn sensor<S: Sensor>(
        &self,
        s: S,
    ) -> Result<StatesEntityResponse, home_assistant_rest::errors::Error> {
        self.get_states_of_entity(s.entity_id()).await
    }
}
