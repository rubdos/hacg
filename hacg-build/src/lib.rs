use std::{fs::File, io::Write};

use anyhow::Context;
use home_assistant_rest::{get::StateEntry, Client};
use quote::{format_ident, quote};

pub async fn build() -> anyhow::Result<()> {
    let filename = std::env::var("OUT_DIR").context("output filename set")?;
    let filename = std::path::PathBuf::from(filename);
    let filename = filename.join("ha.rs");

    let mut f = File::create(filename)?;

    let url = std::env::var("HA_URL").context("HA_URL set")?;
    let token = std::env::var("HA_TOKEN").context("HA_TOKEN set")?;

    let client = Client::new(&url, &token).context("create client")?;
    let walker = Walker::new(client);
    let contents = walker.walk().await?;
    contents.generate_code(&mut f)?;

    Ok(())
}

pub struct Walker {
    client: Client,
}

pub struct HaConfig {
    states: Vec<StateEntry>,
    // services: Vec<ServiceEntry>,
}
impl HaConfig {
    pub fn generate_code(&self, f: &mut impl Write) -> anyhow::Result<()> {
        for state in &self.states {
            generate_code_for_state(f, state)
                .with_context(|| format!("generating code for state {}", state.entity_id))?;
        }
        Ok(())
    }
}

fn generate_code_for_state(f: &mut impl Write, state: &StateEntry) -> anyhow::Result<()> {
    let (entity_type, entity_id) = state.entity_id.split_once('.').ok_or_else(|| {
        anyhow::format_err!("entity id {} does not contain a dot", state.entity_id)
    })?;
    let entity_type = format_ident!("{}", entity_type);
    let entity_name = if entity_id.chars().next().unwrap().is_alphabetic() {
        format_ident!("{}", entity_id)
    } else {
        format_ident!("_{}", entity_id)
    };
    let entity_id = &state.entity_id;
    let syn = quote! {
        ::hacg::state! {
            entity_type: #entity_type,
            entity_name: #entity_name,
            entity_id: #entity_id,
        }
    };
    writeln!(f, "{syn}")?;
    Ok(())
}

impl Walker {
    pub fn new(client: Client) -> Self {
        Self { client }
    }

    pub async fn walk(&self) -> anyhow::Result<HaConfig> {
        Ok(HaConfig {
            states: self.client.get_states().await?,
            // services: self.client.get_services().await?,
        })
    }
}
