use anyhow::Context;
use hacg_build::Walker;
use home_assistant_rest::Client;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    dotenv::dotenv()?;
    env_logger::init();

    let url = std::env::var("HA_URL").context("HA_URL set")?;
    let token = std::env::var("HA_TOKEN").context("HA_TOKEN set")?;

    let client = Client::new(&url, &token).context("create client")?;
    let walker = Walker::new(client);
    let contents = walker.walk().await?;
    contents.generate_code(&mut std::io::stdout())?;

    Ok(())
}
